# Makefile for lxmms2

# Installation directory
PREFIX=/usr/local

ALLFILES=*.c Makefile README COPYING

CFLAGS=-O2 -Wall -fPIC `pkg-config --cflags xmms2-client`
LIBS=`pkg-config --libs xmms2-client` -llirc_client

CC=gcc

OBJS=lxmms2.o

projname:=lxmms2-$(shell awk '/define VERSION/ { print $$3 }' lxmms2.c )

lxmms2: $(OBJS)
	$(CC) $(OBJS) -o lxmms2 $(LIBS)

dist: $(ALLFILES)
	-rm -rf $(projname)
	mkdir $(projname)
	cp -rl --parents $(ALLFILES) $(projname)
	tar chfz $(projname).tar.gz $(projname)
	-rm -rf $(projname)

clean:
	rm -f *.o core *.bak *~ lxmms2

%.o: %.c
	$(CC) $(CFLAGS) -c $<

install: lxmms2
	install -D lxmms2 $(PREFIX)/bin/lxmms2

uninstall:
	-rm $(PREFIX)/bin/lxmms2
